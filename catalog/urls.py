from django.conf.urls import url
from . import views

app_name = 'catalog'

urlpatterns = [
    url('home_init/', views.index, name='index'),
    url('hello_user/', views.hello_user, name='hello_user'),
    url(r'home', views.home, name='home'),
    url(r'data_heroe/(?P<personaje>.*)/', views.data_character, name='data_character'),
    #url(r'search/(?P<search>.*)/', views.search_character, name='search_character'),
]