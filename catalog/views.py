from django.shortcuts import render
from django.views import generic

# Create your views here.

from django.http import HttpResponse
from .services import get_username, get_character, search_character

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

# Especifique su propio nombre/ubicación de plantilla
def hello_user(request):

    params = { 'order': 'desc' }

    # convert reponse data into json
    users =  get_username(params)
    #print("🚀 ~ context:", users)

    return render(request, "hello_user.html", {'users': users})


# Pantalla principal
def home(request):
    search = request.GET.get("search")

    characters = {}
    if search is not None:
        characters =  search_character(search=search)

    return render(request, "home.html", {'characters': characters})

# Especifique su propia ruta data_heroe/<personaje>
def data_character(request, personaje):
    print("🚀 ~ data_character:")

    # convert reponse data into json
    data =  get_character(personaje=personaje)
    data_img = data.get("thumbnail")
    type_img = '/portrait_xlarge'
    image = data_img.get("path")  + type_img + '.' + data_img.get("extension")
    #print("🚀 ~ data:", data)

    return render(request, "data_heroe.html", {'data': data, 'image': image})