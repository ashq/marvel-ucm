import json
import requests

def generate_request(url, params={}):
    response = requests.get(url, params=params)

    if response.status_code == 200:
        return response.json()

def get_username(params={}):
    response = generate_request('https://randomuser.me/api', params)
    if response:
        users = response.get('results')[0]
        users = json.dumps(users)
        users_loads = json.loads(users)
        return users_loads

    return 'ERROR'


############
## Marvel ##
############

apikey = '##############################'
hash = '################################'

# Get a la API MARVEL

def search_character(search):
    print("🚀 ~ search:", search)
    link_character = 'http://gateway.marvel.com/v1/public/characters?nameStartsWith='+ search +'&ts=1000&apikey='+apikey+'&hash='+hash

    response = generate_request(url=link_character)
    if response:
        data_character = response.get('data').get('results')
        # print("🚀 ~ user 36 :", data_character)
        # data_character = json.dumps(data_character)
        # data_character_loads = json.loads(data_character)
        #print("🚀 ~ users 15 :", users_loads)
        return data_character

    return 'ERROR'

def get_character(params={}, personaje=''):
    link_character = 'http://gateway.marvel.com/v1/public/characters?name='+ personaje +'&ts=1000&apikey='+apikey+'&hash='+hash
    response = generate_request(link_character, params)
    if response:
        data_character = response.get('data').get('results')[0]
        # print("🚀 ~ user 13 :", data_character)
        # data_character = json.dumps(data_character)
        # data_character_loads = json.loads(data_character)
        #print("🚀 ~ users 15 :", users_loads)
        return data_character

    return 'ERROR'