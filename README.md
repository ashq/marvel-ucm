# Marvel UCM

## Introducción 

En este proyecto podran realizar la busqueda de distintos personajes del universo de Marvel, personajes de comics, live action y lego. Veran el ID, la descripción y la imagen correspondiente.

En esta versión veran como es consumida la API de Marvel, la podran consultar aqui [Marvel.com](https://developer.marvel.com/ "Title") su documentación.

Este fanatismo me entusiasma a comenzar con esto y seguira para darle crecimiento.

## Tecnologias utilizadas

Aqui estamos usando PYTHON con el framework de DJANGO para realizar el consumo de la api y creación del frontend.  
Lo más importante para la ejecución de este proyecto son las siguientes variables:  

* apikey  
* hash  

las cules se pueden obtener desde [Marvel.com](https://www.marvel.com/signin?referer=https%3A%2F%2Fdeveloper.marvel.com%2Faccount "Title").  

## Ejecución del aplicativo

Para inicializar el proyecto deben ejecutar el siguiente comando:

´´´
$ python3 manage.py runserver
´´´

Dirigirse a la pagina principal 

http://127.0.0.1:8000/catalog/home/

y poder buscar cada personaje.

